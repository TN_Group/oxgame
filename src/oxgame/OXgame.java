/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package oxgame;

import java.util.Scanner;

/**
 *
 * @author HP
 */
public class OXgame {
 private char[][] table;               // game board in 2D array
    private char currentPlayer;           // the current player('x' or 'o')
    private int rows = 3;
    private int columns = 3;
    
    public static Scanner sc = new Scanner(System.in); // the input Scanner
    
    public OXgame() {
        table = new char[rows][columns];           
        currentPlayer = 'X';              // 'x' player first
        initializeTable();
    }
    
    // Set/Reset the board back to all empty values.
    public void initializeTable() {
        for(int i = 0; i < rows; i++) {
            for(int j = 0; j < columns; j++) {
                table[i][j] = '-';
            }
        }
    }
    
    // Print the current board (may be replaced by GUI implementation later)
    public void printTable() {
        for(int i = 0; i < rows; i++) {
            for(int j = 0; j < columns; j++) {
                System.out.print(table[i][j]+" ");
            }
            System.out.println("");
        }
    }
    
    /** Loop through all cells of the board to see if the board is empty.
      * if one is found to be empty (with value '-'), return false, otherwise 
      * return true. */
    public boolean isTableFull() {
        for(int i = 0; i < rows; i++) {
            for(int j = 0; j < columns; j++) {
                if (table[i][j] == '-') {
                    return false;
                }
            }
        }
        return true;
    }
    
    // Returns true if there is a win, false otherwise.
    public boolean checkForWinner() {
        return (checkRows() || checkColumns() || checkDiagonals());
    }
    
    // Loop through rows and see if any are winners.
    private boolean checkRows() {
        for (int i = 0; i < rows; i++) {
            if (table[i][0] != '-' && table[i][0] == table[i][1] && 
                table[i][0] == table[i][2]) {
                return true;
            }
        }
        return false;
    }
    
    // Loop through rows and see if any are winners.
    private boolean checkColumns() {
        for (int i = 0; i < columns; i++) {
            if (table[0][i] != '-' && table[0][i] == table[1][i] && 
                table[0][i] == table[2][i]) {
                return true;
            }
        }
        return false;
    }
    
    // Check the two diagonals to see if either is a win.
    private boolean checkDiagonals() {
        return (table[0][0] != '-' && table[0][0] == table[1][1] 
                && table[0][0] == table[2][2]) 
                ||
               (table[0][2] != '-' && table[0][2] == table[1][1] 
                && table[0][2] == table[2][0]);
                
    }
    
     // Change player marks back and forth.
    public void changePlayer() {
        currentPlayer = (currentPlayer == 'X') ? 'O' : 'X';
    }
    
    public char printPlayer() {
        return currentPlayer;
    }
    
    /** Places a mark at the cell specified by row and col 
      * with the mark of the current player. 
      * Precondition: row and col in range 0-2*/
    public void updateTable() {
        boolean validInput = false;      // if true, the input is valid
        do {
         if (currentPlayer == 'X') {
            System.out.println("turn X ");
            System.out.println("please input row, col : ");
         } else {
            System.out.println("turn O");
            System.out.println("please input row, col : ");
         }
         int row = sc.nextInt() - 1;  // array index starts at 0 instead of 1
         int col = sc.nextInt() - 1;
         
         if (row >= 0 && row < rows && col >= 0 && col < columns 
             && table[row][col] == '-') {
            table[row][col] = currentPlayer;  // update board
            validInput = true;  // input okay, exit loop
         } 
      } while (!validInput);  // repeat until input is valid
    }
    
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
         // Initialize the game-board 
        OXgame t = new OXgame();
        boolean gameStatus = false;             // if true, one player wins
        // play the game until one player wins
         System.out.println("Welcome to OX Game");
         t.printTable();
        do {
            t.updateTable();
            t.printTable();
            // Print message if game over
            if (t.checkForWinner()) {
                System.out.println(">>>"+t.printPlayer()+"  Win<<<");
                gameStatus = true;
            }
            if (t.isTableFull()) {
                System.out.println(">>>Draw<<<");
                gameStatus = true;
            }
            // Switch player
            t.changePlayer();  
        } while (gameStatus == false); // repeat until game over.
         

    }
    
}
